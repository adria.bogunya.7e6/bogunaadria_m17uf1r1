# BogunaAdria_M17UF1R1

![Captura de Pantalla 1](/screenshots/screenshot4.png)

## Descripción

¡Bienvenido a Dimension V! Este es un emocionante videojuego 2D que combina elementos de plataformas, puzles y acción. Sumérgete en un mundo lleno de desafíos y aventuras mientras controlas a nuestro intrépido héroe en su búsqueda para llegar a la meta y recolectar valiosas monedas.


## Características Principales

- **$\mathcal{\color{cyan}{Mecánica}}$ $\mathcal{\color{cyan}{de}}$ $\mathcal{\color{cyan}{Invertir}}$ $\mathcal{\color{cyan}{la}}$ $\mathcal{\color{cyan}{Gravedad:}}$** 🙃

  Experimenta una mecánica única que te permite cambiar la orientación de la gravedad para explorar diferentes pantallas y superar obstáculos.

- **$\mathcal{\color{cyan}{Niveles}}$ $\mathcal{\color{cyan}{Intrincados:}}$** 🧩 

    Enfréntate a niveles cuidadosamente diseñados que desafiarán tus habilidades de plataformas y rompecabezas.

- **$\mathcal{\color{cyan}{Enemigos}}$ $\mathcal{\color{cyan}{y}}$ $\mathcal{\color{cyan}{Proyectiles:}}$** 👾 

    Evita enemigos y proyectiles hostiles que intentarán detenerte en tu camino hacia la victoria.

- **$\mathcal{\color{cyan}{Recolección}}$ $\mathcal{\color{cyan}{de}}$ $\mathcal{\color{cyan}{Monedas:}}$** 📀 

    ¡No olvides recoger todas las monedas que puedas para ganar puntos y desbloquear contenido adicional!

- **$\mathcal{\color{cyan}{Ambiente}}$ $\mathcal{\color{cyan}{Visualmente}}$ $\mathcal{\color{cyan}{Atractivo:}}$** ✨ 

    Sumérgete en un mundo vibrante y colorido con gráficos 2D de alta calidad que hacen que la experiencia de juego sea aún más emocionante.

## Estilo Visual

El juego presenta gráficos en 2D con un estilo artístico que combina elementos espaciales y toques de mazmorra para crear un ambiente único.


## Controles

- **$\text{\color{green}{Movimiento:}}$** Utiliza las teclas $\text{\color{green}{A}}$ y $\text{\color{green}{D}}$ o las flechas $\text{\color{green}{←}}$ y $\text{\color{green}{→}}$ del teclado para mover al personaje hacia la izquierda y derecha.

- **$\text{\color{orange}{Invertir Gravedad:}}$** Usa las teclas $\text{\color{orange}{W}}$, $\text{\color{orange}{D}}$ o la barra espaciadora $\text{\color{orange}{(Space)}}$ para invertir la gravedad y cambiar la orientación del personaje.

- **$\text{\color{cyan}{Pausar Juego:}}$** Pulsa la tecla $\text{\color{cyan}{"Esc"}}$ para pausar el juego y acceder al menú de pausa.

- **$\text{\color{magenta}{Navegación en Menús:}}$** En los menús de Inicio, Pausa y Game Over, utiliza las teclas de flecha $\text{\color{magenta}{↑}}$ y $\text{\color{magenta}{↓}}$ para navegar entre las opciones. Presiona $\text{\color{violet}{"Enter"}}$ para confirmar tus selecciones.

## Capturas de Pantalla

![Captura de Pantalla 2](/screenshots/screenshot1.png)


## Género y Temática

**$\mathcal{\color{lightblue}{Género:}}$** Plataformas / Puzles

**$\mathcal{\color{lightblue}{Temática:}}$** "Dimension V" es un emocionante juego de plataformas que te desafía a enfrentar obstáculos y acertijos mientras navegas por un mundo espacial con un toque de mazmorra. La jugabilidad se centra en la inversión de gravedad, lo que añade una mecánica única al juego. A medida que avanzas a través de los niveles, te encontrarás con diversas situaciones:

  1. **$\mathcal{\color{lightgreen}{Niveles}}$ $\mathcal{\color{lightgreen}{de}}$ $\mathcal{\color{lightgreen}{Aprendizaje:}}$** Comienza con la tarea de moverte hacia un lado para familiarizarte con los controles básicos.
  
  2. **$\mathcal{\color{lightgreen}{Evitar}}$ $\mathcal{\color{lightgreen}{Obstáculos}}$ $\mathcal{\color{lightgreen}{e}}$ $\mathcal{\color{lightgreen}{Invertir}}$ $\mathcal{\color{lightgreen}{la}}$ $\mathcal{\color{lightgreen}{Gravedad:}}$** En este punto, debes esquivar obstáculos y aprender a invertir la gravedad para superar desafíos.
  
  3. **$\mathcal{\color{lightgreen}{Cambio}}$ $\mathcal{\color{lightgreen}{de}}$ $\mathcal{\color{lightgreen}{Dirección:}}$** Aquí, experimentarás un cambio de dirección que te obligará a adaptarte rápidamente.
  
  4. **$\mathcal{\color{lightgreen}{Evitar}}$ $\mathcal{\color{lightgreen}{Proyectiles:}}$** Enfrenta la amenaza de proyectiles y demuestra tus habilidades de evasión.
  
  5. **$\mathcal{\color{lightgreen}{Evitar}}$ $\mathcal{\color{lightgreen}{Enemigos:}}$** Desafía a un enemigo que intenta interceptarte mientras avanzas.
  
  6. **$\mathcal{\color{lightgreen}{Más}}$ $\mathcal{\color{lightgreen}{Obstáculos:}}$** Nuevos obstáculos complican aún más tu misión.
  
  7. **$\mathcal{\color{lightgreen}{Siguiente}}$ $\mathcal{\color{lightgreen}{Nivel:}}$** Supera todos los desafíos y avanza al siguiente nivel. La dificultad seguirá aumentando a medida que progresas.


## Instalación

1. Clona o descarga el repositorio.
2. Abre el proyecto en Unity.
3. Ejecuta el juego desde el editor o compila para la plataforma de tu elección.

## Requisitos del Sistema

- Unity 2022.3.9f1
- Sistema Operativo: Windows 10 o macOS 10.14

## Contribución

Si deseas contribuir al desarrollo de Dimension V, ¡estoy abiertos a colaboradores! Siéntete libre de enviar pull requests o informar problemas.

## Licencia

[![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)  

Este juego se distribuye con Licencia pública.

## Contacto

- adria.bogunat@gmail.com
- [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=normal&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/adriaboguna/)

##
¡Espero que disfrutes jugando a Dimension V tanto como yo disfrute creándolo! 🕹 

¡Diviértete y buena suerte! 😎👍
