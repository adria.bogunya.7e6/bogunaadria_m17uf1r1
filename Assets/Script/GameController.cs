﻿/*
 * Autor: Adrià Boguñá Torres
 * Date: 20/09/2023
 * Description: S'encarrega de gestionar aspectes globals del joc, com ara el recompte de col·leccionables, 
 * el control de checkpoints i el moviment de la càmera entre habitacions. També supervisa les condicions de victòria o derrota al joc.
 */

using UnityEngine;
using UnityEngine.UI; // Proporciona components i funcionalitats que et permeten dissenyar i controlar elements de la interfície d'usuari, com ara botons, text, imatges..

public class GameController : MonoBehaviour
{
    public static int position;
    public int trinketsToVictory = 5;
    public GameObject activeCheckpoint;
    public Text trinketCount;
    public Text trinketText;

    private int trinketsCollected = 0;

    // Es crida cada cop que el jugador recol·lecta un trinket (moneda). 
    public void AddTrinket() 
    {
        trinketsCollected++;

        // Comprovem si hem aconseguit l'objectiu.
        if (CheckVictory() == false) 
        { 
            trinketCount.text = trinketsCollected.ToString();
        }
        else
        {
            trinketCount.text = "";
            trinketText.text = "All coins collected!!";
        }
    }

    // Mètode privat que verifica si s'han recol·lectat tots els trinkets. 
    private bool CheckVictory() 
    {
        return (trinketsCollected == trinketsToVictory);
    }

    // S'utilitza per activar un nou checkpoint.
    public void ActivateCheckpoint(GameObject newCheckpoint) 
    {
        // Desactiva el checkpoint antic (si existeix).
        if (activeCheckpoint)
        {
            activeCheckpoint.GetComponent<Checkpoint>().Deactivate();
        }

        // Activa el nou checkpoint passat com a argument.
        activeCheckpoint = newCheckpoint;
        activeCheckpoint.GetComponent<Checkpoint>().Activate();
    }

    // Es crida per moure la càmera a la posició de respawn.
    public void MoveCameraToRespawn()
    {
        // Donem per fet, que el respawn és un fill d'una habitació i obté la posició de l'habitació per ajustar la posició de la càmera.
        Vector2 newPosition = activeCheckpoint.transform.parent.transform.position;
        MoveCamera(newPosition);
    }

    // S'utilitza per moure la càmera a una nova posició específica. 
    public void MoveCamera(Vector2 newPosition) 
    {
        Camera.main.transform.position = new Vector3(newPosition.x, newPosition.y, Camera.main.transform.position.z);
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
