/*
 * Autor: Adri� Bogu�� Torres
 * Date: 25/09/2023
 * Description: defineix un script de Spawn (generaci�) a Unity.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spauner : MonoBehaviour
{
    public int position;
    GameObject player;

    private void Awake()
    {
        if(GameController.position == position)
        {
            player=GameObject.FindGameObjectWithTag("Player"); // Busquem l'objecte del jugador per la seva etiqueta i l'emmagatzemem a la variable "player".
            player.transform.position = transform.position;
        }
    }
}
