﻿/*
 * Autor: Adrià Boguñá Torres
 * Date: 20/09/2023
 * Description: Representa un objecte col·leccionable al joc. 
 * Quan el jugador xoca amb un objecte d'aquesta classe, es reprodueix un so i l'objecte es destrueix. 
 * A més, s'anomena el mètode AddTrinket al GameController per registrar la recol·lecció del trinket per part del jugador. 
 * Aquesta classe gestiona la interacció i la recol·lecció d'aquests objectes en el joc.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trinket : MonoBehaviour
{

    public GameController gameController;
    public AudioClip collected;

    private AudioSource audioSource;


    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Aquest mètode es diu quan un objecte colisiona amb el trinket
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            audioSource.PlayOneShot(collected);
            
            gameController.AddTrinket(); // Notifiquem al controlador del joc que s'ha recol·lectat un trinket
            Destroy(gameObject);
        }
    }
}
