/*
 * Autor: Adri� Bogu�� Torres
 * Date: 26/09/2023
 * Description: controla un portal a Unity que permet al jugador canviar d'una escena a una altra.
 */

using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalController : MonoBehaviour
{
    public string destinationSceneName;
    public string destinationPortalName;
    public int position;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // Emmagatzema el nom del portal de destinaci� a PlayerPrefs.
            PlayerPrefs.SetString("DestinationPortalName", destinationPortalName);
            PlayerPrefs.Save();

            GameController.position = position;

            // C�rrega l'escena de destinaci�.
            SceneManager.LoadScene(destinationSceneName);
        }
    }
}


