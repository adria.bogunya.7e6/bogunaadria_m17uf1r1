/*
 * Autor: Adri� Bogu�� Torres
 * Date: 20/09/2023
 * Description: controla el moviment d'una plataforma m�bil.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform platform;
    public Transform startPoint;
    public Transform endPoint;
    public float constantSpeed = 1.5f;

    // Sexecuta en cada fotograma del joc i mou la plataforma cap al seu objectiu actual utilitzant Vector2.MoveTowards.
    private void Update()
    {
        float step = constantSpeed * Time.deltaTime;
        platform.position = Vector2.MoveTowards(platform.position, currentMovementTarget(), step);

        if (Vector2.Distance(platform.position, endPoint.position) < 0.01f)
        {
            SwapTargets();
        }
        else if (Vector2.Distance(platform.position, startPoint.position) < 0.01f)
        {
            SwapTargets();
        }
    }

    // Determina lobjectiu de moviment actual de la plataforma en funci� de la seva posici� actual.
    Vector2 currentMovementTarget()
    {
        if (Vector2.Distance(platform.position, startPoint.position) < 0.01f)
        {
            return endPoint.position;
        }
        else
        {
            return startPoint.position;
        }
    }

    // Intercanvia els punts dinici i fi de la plataforma.
    void SwapTargets()
    {
        Vector2 temp = startPoint.position;
        startPoint.position = endPoint.position;
        endPoint.position = temp;
    }

    // S'utilitza per dibuixar l�nies Gizmos a l'editor d'Unity per visualitzar la ruta de moviment de la plataforma.
    private void OnDrawGizmos()
    {
        if (platform != null && startPoint != null && endPoint != null)
        {
            Gizmos.DrawLine(platform.transform.position, startPoint.position);
            Gizmos.DrawLine(platform.transform.position, endPoint.position);
        }
    }
}
