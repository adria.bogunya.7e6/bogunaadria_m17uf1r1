/*
 * Autor: Adri� Bogu�� Torres
 * Date: 25/09/2023
 * Description: controla el canvi de nivell quan el jugador toca un objecte especial.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelUp : MonoBehaviour
{
    private bool hasPlayedAnimation = false;
    public GameObject Panel;

    // Es verifica si el jugador ha tocat l'objecte especial (identificat per l'etiqueta Player) i si l'animaci� ja s'ha reprodu�t.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player") && !hasPlayedAnimation)
        {
            UnlockNewLevel();

            if (Panel != null)
            {
                Panel.SetActive(true);
                Animation animation = Panel.GetComponent<Animation>();

                if (animation != null)
                {
                    animation.Play("LevelUp");
                }
            }

            // Estableix la variable hasPlayedAnimation a true per evitar que es torni a reproduir.
            hasPlayedAnimation = true;

            StartCoroutine(ChangeSceneAfterAnimation());
        }
    }

    // Es crida per desbloquejar el nivell seg�ent en funci� de l'�ndex actual de l'escena activa i portar un registre dels nivells desbloquejats.
    void UnlockNewLevel()
    {
        Debug.Log("Before unlocking: Reached Index: " + PlayerPrefs.GetInt("ReachedIndex"));
        Debug.Log("Before unlocking: Unlocked Level: " + PlayerPrefs.GetInt("UnlockedLevel", 1));

        if (SceneManager.GetActiveScene().buildIndex >= PlayerPrefs.GetInt("ReachedIndex"))
        {
            PlayerPrefs.SetInt("ReachedIndex", SceneManager.GetActiveScene().buildIndex + 1);
            PlayerPrefs.SetInt("UnlockedLevel", PlayerPrefs.GetInt("UnlockedLevel", 1) + 1);
            PlayerPrefs.Save();
        }

        Debug.Log("After unlocking: Reached Index: " + PlayerPrefs.GetInt("ReachedIndex"));
        Debug.Log("After unlocking: Unlocked Level: " + PlayerPrefs.GetInt("UnlockedLevel", 1));
    }

    // S'utilitza la seg�ent corrutina, per esperar la durada de l animaci� abans de canviar d escena. S'ajusta el temps d'espera segons la durada de l'animaci�.
    private IEnumerator ChangeSceneAfterAnimation()
    {
        float animationDuration = 8.5f;
        Debug.Log("Waiting for animation: " + animationDuration + " seconds");
        yield return new WaitForSeconds(animationDuration);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
