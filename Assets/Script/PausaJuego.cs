/*
 * Autor: Adri� Bogu�� Torres
 * Date: 24/09/2023
 * Description: permet la funcionalitat de pausa.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausaJuego : MonoBehaviour
{
    public GameObject pauseMenu;
    public GameObject pauseButton;

    [SerializeField] GameObject resumeButton;
    [SerializeField] GameObject restartButton;
    [SerializeField] GameObject quitButton;
    [SerializeField] MenuButtonController menuButtonController;

    // Verifica si s'ha pressionat la tecla Escape i crida a PauseGame()
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }
    }

    // Pausa el joc ajustant Time.timeScale a 0, cosa que atura la simulaci� del temps del joc. Despr�s, activa el men� de pausa i els botons relacionats mentre desactiva el bot� de pausa.
    public void PauseGame()
    {
        Time.timeScale = 0f;
        pauseMenu.SetActive(true);
        resumeButton.SetActive(true);
        restartButton.SetActive(true);
        quitButton.SetActive(true);
        menuButtonController.index = 0;
    }

    // Repr�n el joc ajustant Time.timeScale a 1.
    public void ResumeGame()
    {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        resumeButton.SetActive(false);
        restartButton.SetActive(false);
        quitButton.SetActive(false);
    }

    // Reinicieu el joc carregant novament l'escena actual. Assegura que el temps del joc estigui en curs (Time.timeScale = 1) per reprendre el joc despr�s del reinici.
    public void RestartGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Surt de l'aplicaci�
    public void Quit()
    {
        Application.Quit();
        Debug.Log("You've just exit");
    }
}
