﻿/*
 * Autor: Adrià Boguñá Torres
 * Date: 20/09/2023
 * Description: defineix el component "Transition" a Unity.
 */

using UnityEngine;

public class Transition : MonoBehaviour
{
    public bool verticalTransition = false; // Determina si la transició és vertical (true) o horitzontal (false).

    // Són referències a objectes de tipus "Transform" que representen les ubicacions de les sales.
    public Transform leftRoom;
    public Transform rightRoom;
    public Transform topRoom;
    public Transform bottomRoom;
}
