/*
 * Autor: Adri� Bogu�� Torres
 * Date: 20/09/2023
 * Description: Representa el personatge jugable en el joc i controla el seu moviment, interaccions i estats,
 * com saltar, moure's, col�lisions i gestionar la seva vida en el joc.
 */

using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Public members
    public float speed;
    public float gravity;
    public float checkRadius;
    public float waitOnDeath;
    public bool isDebug;
    public GameController gameController;
    public LayerMask whatIsGround;
    public GameObject spawnPoint;

    // Audio clips
    public AudioClip jump;
    public AudioClip death;
    public AudioClip trinketOrCheckpoint;

    // Components
    private Rigidbody2D rb;
    private BoxCollider2D mCollider;
    private Animator playerAnimator;
    private AudioSource audioSource;

    // Public members
    [HideInInspector]
    public bool facingRight = true;
    [HideInInspector]
    public bool facingUp = true;

    // Private members
    float moveInput;
    bool isGrounded = true;
    bool isDying = false;

    void Start()
    {
        // Obtenim refer�ncies als components del jugador.
        rb = GetComponent<Rigidbody2D>();
        mCollider = GetComponent<BoxCollider2D>();
        playerAnimator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    // S'utilitza per detectar si el jugador ha pressionat les tecles W, S o Espai per invertir la gravetat del personatge quan �s a terra.
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.Space)) && isGrounded && !isDying)
        {
            audioSource.PlayOneShot(jump);
            gravity = -gravity;
            FlipVertically();
        }
    }

    // S'encarrega de controlar el moviment horitzontal del jugador, la detecci� de si �s a terra i l'animaci� de caminar. Tamb� maneja la inversi� de l'sprite quan el jugador canvia de direcci�.
    void FixedUpdate()
    {
        if (!isDying)
        {
            isGrounded = CheckIsGrounded();
            moveInput = Input.GetAxis("Horizontal");

            rb.velocity = new Vector2(moveInput * speed, isGrounded ? 0.0f : gravity);

            playerAnimator.SetBool("isWalking", false);

            if (moveInput > 0.0f || moveInput < 0.0f)
            {
                playerAnimator.SetBool("isWalking", true);
            }

            if ((facingRight == false && moveInput > 0) || (facingRight == true && moveInput < 0))
            {
                FlipHorizontally();
            }
        }
    }

    // (Enter2D i Exit2D) Gestionen les col�lisions amb objectes perillosos i plataformes m�bils, respectivament.
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Danger"))
        {
            StartCoroutine(ManageDeath());
        }

        if (collision.gameObject.tag == "PlataformaMovil")
        {
            transform.parent = collision.transform;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "PlataformaMovil")
        {
            transform.parent = null;
        }
    }

    // Maneja les col�lisions amb punts de control, transicions d'habitacions i trinkets (objectes col�leccionables).
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Respawn"))
        {
            Checkpoint checkpoint = collision.gameObject.GetComponent<Checkpoint>();

            if (!checkpoint.isActive)
            {
                audioSource.PlayOneShot(trinketOrCheckpoint);
                gameController.ActivateCheckpoint(collision.gameObject);
            }
        }
        else if (collision.gameObject.CompareTag("Transition"))
        {
            ChangeRoom(collision.GetComponent<Transition>());
        }
        else if (collision.gameObject.CompareTag("Trinket"))
        {
            audioSource.PlayOneShot(trinketOrCheckpoint);
            Destroy(collision.gameObject);

            gameController.AddTrinket();
        }
    }

    // Sutilitza per gestionar les sortides de les transicions dhabitacions.
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Transition"))
        {
            ChangeRoom(collision.GetComponent<Transition>());
        }
    }

    // Determina la posici� de la c�mera en canviar duna habitaci� a una altra en el joc.
    void ChangeRoom(Transition transition)
    {
        if (transition.verticalTransition)
        {
            if (facingUp)
            {
                // Si estem orientats cap amunt, fem servir la posici� de la part inferior de l'habitaci�.
                Vector3 newCameraPos = new Vector3(transition.bottomRoom.position.x, transition.bottomRoom.position.y, Camera.main.transform.position.z);
                gameController.MoveCamera(newCameraPos);
            }
            else
            {
                Vector3 newCameraPos = new Vector3(transition.topRoom.position.x, transition.topRoom.position.y, Camera.main.transform.position.z);
                gameController.MoveCamera(newCameraPos);
            }
        }
        else
        {
            if (facingRight)
            {
                Vector3 newCameraPos = new Vector3(transition.rightRoom.position.x, transition.rightRoom.position.y, Camera.main.transform.position.z);
                gameController.MoveCamera(newCameraPos);
            }
            else
            {
                Vector3 newCameraPos = new Vector3(transition.leftRoom.position.x, transition.leftRoom.position.y, Camera.main.transform.position.z);
                gameController.MoveCamera(newCameraPos);
            }
        }
    }

    // Verifica si el jugador �s a terra utilitzant un raig de col�lisi�.
    bool CheckIsGrounded()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(mCollider.bounds.center, mCollider.bounds.size, 0f, facingUp ? Vector2.down : Vector2.up, 0.1f, whatIsGround);

        PaintDebugBox(raycastHit);

        return raycastHit.collider != null;
    }

    // S'utilitza per dibuixar un raig de col�lisi� a l'editor d'Unity si el mode de depuraci� est� habilitat.
    void PaintDebugBox(RaycastHit2D raycastHit)
    {
        if (isDebug)
        {
            Color rayColor;

            if (raycastHit.collider != null)
            {
                rayColor = Color.green;
            }
            else
            {
                rayColor = Color.red;
            }

            Vector2 abajo = facingUp ? Vector2.down : Vector2.up;

            Debug.DrawRay(mCollider.bounds.center + new Vector3(mCollider.bounds.extents.x, 0), abajo * (mCollider.bounds.extents.y + 0.1f), rayColor);
            Debug.DrawRay(mCollider.bounds.center - new Vector3(mCollider.bounds.extents.x, 0), abajo * (mCollider.bounds.extents.y + 0.1f), rayColor);

            if (facingUp)
            {
                Debug.DrawRay(mCollider.bounds.center - new Vector3(mCollider.bounds.extents.x, mCollider.bounds.extents.y + 0.1f), Vector3.right * (mCollider.bounds.size.y), rayColor);
            }
            else
            {
                Debug.DrawRay(mCollider.bounds.center + new Vector3(mCollider.bounds.extents.x, mCollider.bounds.extents.y + 0.1f), -Vector3.right * (mCollider.bounds.size.y), rayColor);
            }

            Debug.Log(raycastHit.collider);
        }
    }

    // Inverteixen l'sprite del jugador horitzontal i verticalment, respectivament.
    void FlipHorizontally()
    {
        facingRight = !facingRight;
        Vector2 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }

    void FlipVertically()
    {
        facingUp = !facingUp;
        Vector2 Scaler = transform.localScale;
        Scaler.y *= -1;
        transform.localScale = Scaler;
    }

    // Controla la seq��ncia d'esdeveniments quan el jugador mor, incloent-hi l'animaci� de mort, el reinici de la posici� i la inversi� de la gravetat.
    IEnumerator ManageDeath()
    {
        isDying = true;
        playerAnimator.SetBool("isDying", true);
        audioSource.PlayOneShot(death);

        rb.Sleep();

        yield return new WaitForSeconds(waitOnDeath);
        playerAnimator.SetBool("isDying", false);

        if (gravity > 0.0f)
        {
            FlipVertically();
            gravity *= -1;
        }

        transform.position = gameController.activeCheckpoint.transform.position;

        gameController.MoveCameraToRespawn();

        rb.WakeUp();
        isDying = false;
    }
}
