/*
 * Autor: Adri� Bogu�� Torres
 * Date: 23/09/2023
 * Description: controla el moviment d'un enemic d'una banda a l'altra, amb pauses entre els moviments. L'enemic es volteja quan aconsegueix un punt i alterna entre dos punts de refer�ncia.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoMovimiento : MonoBehaviour
{
    public float velocidad = 2.0f;
    public float distancia = 5.0f;
    public float tiempoEspera = 5.0f;

    private Vector3 puntoA;
    private Vector3 puntoB;
    private bool moviendoseHaciaB = false;
    private bool esperando = false;
    private float tiempoEsperado;
    private Animator animador;

    // Inicialitzar els punts de refer�ncia i obtenir el component Animator de l'enemic.
    void Start()
    {
        puntoA = transform.position;
        puntoB = puntoA + new Vector3(distancia, 0, 0);
        animador = GetComponent<Animator>();
    }

    // L'enemic es mou d'un punt a un altre, espera durant un temps i despr�s inverteix la direcci� de moviment quan es completa l'espera.
    void Update()
    {
        if (!esperando)
        {
            if (moviendoseHaciaB)
            {
                transform.position = Vector3.MoveTowards(transform.position, puntoB, velocidad * Time.deltaTime);

                if (Vector3.Distance(transform.position, puntoB) < 0.1f)
                {
                    Flip();
                    esperando = true;
                    tiempoEsperado = Time.time + tiempoEspera;
                    animador.SetBool("Idle", true);
                }
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, puntoA, velocidad * Time.deltaTime);

                if (Vector3.Distance(transform.position, puntoA) < 0.1f)
                {
                    Flip();
                    esperando = true;
                    tiempoEsperado = Time.time + tiempoEspera;
                    animador.SetBool("Idle", true);
                }
            }
        }
        else
        {
            if (Time.time >= tiempoEsperado)
            {
                esperando = false;
                moviendoseHaciaB = !moviendoseHaciaB;
                animador.SetBool("Idle", false);
            }
        }
    }

    // Inverteix l'escala a l'eix X de l'enemic, cosa que provoca que es voltegi en la direcci� oposada.
    void Flip()
    {
        Vector3 escala = transform.localScale;
        escala.x *= -1;
        transform.localScale = escala;
    }
}
