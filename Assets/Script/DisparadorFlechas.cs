/*
 * Autor: Adri� Bogu�� Torres
 * Date: 22/09/2023
 * Description: maneja la generaci� de fletxes en un joc.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparadorFlechas : MonoBehaviour
{
    public GameObject flechaPrefab;
    public Transform firePoint;
    public float velocidadFlecha = 5f; 
    public float tiempoEntreRafagas = 1f; 
    public int cantidadFlechasPorRafaga = 3; 
    public float tiempoEntreFlechas = 0.2f;
    public float tiempoDeVidaDeFlecha = 7f;

    // Pila de fletxes disponibles
    private Stack<GameObject> flechasDisponibles = new Stack<GameObject>();

    // Aquest m�tode s'anomena al comen�ament i s'utilitza per iniciar la generaci� de fletxes.
    private void Start()
    {
        StartCoroutine(DispararRafagas());
    }

    // controla la generaci� de r�fegues de fletxes a intervals regulars. Genera un nombre de fletxes per r�fega, esperant un temps entre cada tret i un temps entre cada r�fega.
    IEnumerator DispararRafagas()
    {
        while (true)
        {
            for (int i = 0; i < cantidadFlechasPorRafaga; i++)
            {
                DispararFlecha();
                yield return new WaitForSeconds(tiempoEntreFlechas);
            }

            yield return new WaitForSeconds(tiempoEntreRafagas);
        }
    }

    // Aqu� es dispara una fletxa. Si hi ha una fletxa disponible a la pila, es reutilitza; altrament, es crea una nova fletxa.
    void DispararFlecha()
    {
        GameObject flecha = GetFlechaDisponible();
        if (flecha == null)
        {
            flecha = Instantiate(flechaPrefab, firePoint.position, firePoint.rotation);
        }
        else
        {
            flecha.transform.position = firePoint.position;
            flecha.transform.rotation = firePoint.rotation;
        }

        Rigidbody2D rb = flecha.GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * velocidadFlecha;

        StartCoroutine(DestruirFlecha(flecha));
    }

    // Busca una fletxa disponible a la pila de fletxes. Si n'hi ha una de disponible, la treu de la pila i l'activa.
    GameObject GetFlechaDisponible()
    {
        if (flechasDisponibles.Count > 0)
        {
            GameObject flecha = flechasDisponibles.Pop();
            flecha.SetActive(true);
            return flecha;
        }
        return null;
    }

    // S'encarrega de desactivar una fletxa despr�s d'un temps de vida espec�fic i la torna a la pila de fletxes disponibles.
    IEnumerator DestruirFlecha(GameObject flecha)
    {
        yield return new WaitForSeconds(tiempoDeVidaDeFlecha);
        flecha.SetActive(false);
        flechasDisponibles.Push(flecha);
    }
}
