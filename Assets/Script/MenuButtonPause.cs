﻿/*
 * Autor: Adrià Boguñá Torres
 * Date: 24/09/2023
 * Description: controla els botons en un menú de pausa.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuButtonPause : MonoBehaviour
{
    [SerializeField] MenuButtonController menuButtonController;
    [SerializeField] Animator animator;
    [SerializeField] AnimatorFunctions animatorFunctions;
    [SerializeField] int thisIndex;

    public Button resumeButton;
    public Button restartButton;
    public Button quitButton;

    private bool canInteract;

    // Es fa servir per controlar si es permet la interacció amb els botons.
    private void Start()
    {
        canInteract = true;
    }

    // Es verifica si es permet la interacció.
    /// <summary>
    /// Si l'índex del botó actual coincideix amb l'índex del botó seleccionat al controlador del menú, 
    /// s'activa l'animador i es verifica si s'ha premut el botó Submit. 
    /// Si es pressiona, es crida al mètode HandleButtonPress().
    /// </summary>
    public void Update()
    {
        if (!canInteract) return;

        if (menuButtonController.index == thisIndex)
        {
            animator.SetBool("selected", true);
            if (Input.GetAxis("Submit") == 1)
            {
                animator.SetBool("pressed", true);
                HandleButtonPress();
            }
            else if (animator.GetBool("pressed"))
            {
                animator.SetBool("pressed", false);
                animatorFunctions.disableOnce = true;
            }
        }
        else
        {
            animator.SetBool("selected", false);
        }
    }

    // Verifica l'índex del botó actual i, en funció del valor, realitza l'acció corresponent.
    public void HandleButtonPress()
    {
        if (thisIndex == 0) // Botó "RESUME"
        {
            if (resumeButton != null)
            {
                resumeButton.onClick.Invoke();
                canInteract = false;
            }
        }
        else if (thisIndex == 1) // Botó "RESTART"
        {
            if (restartButton != null)
            {
                restartButton.onClick.Invoke();
                canInteract = false;
            }
        }
        else if (thisIndex == 2) // Botó "QUIT"
        {
            if (quitButton != null)
            {
                quitButton.onClick.Invoke();
                canInteract = false;
            }
        }
    }
}

