/*
 * Autor: Adri� Bogu�� Torres
 * Date: 23/09/2023
 * Description: controla un men� de nivells.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenu : MonoBehaviour
{
    public Button[] buttons;
    public GameObject levelButtons;

    // Es recopilen els botons del men� de nivells i es determina quin �s el nivell desbloquejat actual. La informaci� sobre el nivell desbloquejat s'emmagatzema a PlayerPrefs.
    // Es desactiven tots els botons del men� de nivells i despr�s s'activen els botons corresponents als nivells desbloquejats. Aix� permet que nom�s els nivells desbloquejats siguin seleccionables.
    private void Awake()
    {
        ButtonsToArray();
        int unlockedLevel = PlayerPrefs.GetInt("UnlockedLevel", 1);

        Debug.Log("Unlocked Level: " + unlockedLevel);

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = false;
        }
        for (int i = 0; i < unlockedLevel; i++)
        {
            buttons[i].interactable = true;
        }
    }

    // Es crida quan se selecciona un nivell. Carrega l'escena corresponent al nivell seleccionat.
    public void OpenLevel(int levelId)
    {
        string levelName = "Level " + levelId;
        SceneManager.LoadScene(levelName);
    }

    // Converteix els botons del men� de nivells en un arranjament per facilitar-ne la manipulaci�.
    void ButtonsToArray()
    {
        int childCount = levelButtons.transform.childCount;
        buttons = new Button[childCount];
        for (int i = 0; i < childCount; i++)
        {
            buttons[i] = levelButtons.transform.GetChild(i).gameObject.GetComponent<Button>();
        }
    }
}
