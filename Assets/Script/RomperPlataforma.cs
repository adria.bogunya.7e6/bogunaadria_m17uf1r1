/*
 * Autor: Adri� Bogu�� Torres
 * Date: 23/09/2023
 * Description: controla una plataforma que es pot trencar quan el jugador xoca amb ella.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RomperPlataforma : MonoBehaviour
{
    public Animator plataformaAnimator;
    public float tiempoDeEspera = 1f;
    public float tiempoDeEsperaCaida = 0.5f;

    private Collider2D plataformaCollider;

    private void Start()
    {
        plataformaCollider = GetComponent<Collider2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(ActivarRomperPlataforma());
            
        }
    }

    // Corutina per controlar el trencament de la plataforma.
    IEnumerator ActivarRomperPlataforma()
    {
        yield return new WaitForSeconds(tiempoDeEspera);
        plataformaAnimator.SetTrigger("Romper");

        yield return new WaitForSeconds(tiempoDeEsperaCaida); // Esperem un temps addicional despr�s del trencament.
        plataformaCollider.enabled = false;

        Destroy(gameObject);

    }
}
