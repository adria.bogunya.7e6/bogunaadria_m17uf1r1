﻿/*
 * Autor: Adrià Boguñá Torres
 * Date: 20/09/2023
 * Description: controla el comportament d'un checkpoint al joc.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public Color color;
    public Color activeColor;

    private SpriteRenderer spriteRenderer;
    [HideInInspector]
    public bool isActive;

    // Inicialitzar el checkpoint amb el color i l'estat inicials.
    void Start()
    {
        isActive = false;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.color = color;
    }

    // Activar el checkpoint canviant el seu color a "activeColor" i marcant-lo com a actiu.
    public void Activate()
    {
        if (!isActive)
        {
            isActive = true;
            spriteRenderer.color = activeColor;
        }
    }

    // Desactiva el checkpoint canviant el seu color al color original i marcant-lo com a inactiu.
    public void Deactivate()
    {
        if(isActive)
        {
            isActive = false;
            spriteRenderer.color = color;
        }
    }
}
