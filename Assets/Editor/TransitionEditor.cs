﻿/*
 * Autor: Adrià Boguñá Torres
 * Date: 26/09/2023
 * Description: Aquest codi defineix un editor personalitzat per al component "Transition" a Unity. 
 * L'editor permet modificar les propietats del component de manera més amigable per a l'usuari. 
 * Permet canviar la direcció de la transició (vertical o horitzontal) i seleccionar 
 * les sales superiors i inferiors o les sales esquerra i dreta segons la direcció de la transició.
 */

using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Transition))]
public class TransitionEditor : Editor
{
    // Sobreescrivim el mètode OnInspectorGUI per personalitzar la interfície del component a l'inspector.
    public override void OnInspectorGUI()
    {
        Transition myScript = target as Transition;

        myScript.verticalTransition = EditorGUILayout.Toggle("Vertical Transition", myScript.verticalTransition);

        if (myScript.verticalTransition)
        {
            myScript.topRoom = EditorGUILayout.ObjectField("Top Room", myScript.topRoom, typeof(Transform), true) as Transform;
            myScript.bottomRoom = EditorGUILayout.ObjectField("Bottom Room", myScript.bottomRoom, typeof(Transform), true) as Transform;
        }
        else
        {
            myScript.leftRoom = EditorGUILayout.ObjectField("Left Room", myScript.leftRoom, typeof(Transform), true) as Transform;
            myScript.rightRoom = EditorGUILayout.ObjectField("Right Room", myScript.rightRoom, typeof(Transform), true) as Transform;
        }
    }
}

