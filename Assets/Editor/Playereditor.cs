/*
 * Autor: Adri� Bogu�� Torres
 * Date: 26/09/2023
 * Description: Aquest codi defineix un editor personalitzat per al component "Player" a Unity. 
 * Permet configurar par�metres relacionats amb la f�sica, el so, el control i la direcci� del personatge, 
 * cosa que facilita la configuraci� d'aquest component a l'inspector.
 */

using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Player))]
public class PlayerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        // Obtenim una refer�ncia al component "Player" que s'est� inspeccionant.
        Player myScript = target as Player;

        myScript.speed = EditorGUILayout.FloatField("Speed", myScript.speed);
        myScript.gravity = EditorGUILayout.FloatField("Gravity", myScript.gravity);
        myScript.checkRadius = EditorGUILayout.FloatField("Check Radius", myScript.checkRadius);
        myScript.waitOnDeath = EditorGUILayout.FloatField("Wait on Death", myScript.waitOnDeath);
        myScript.isDebug = EditorGUILayout.Toggle("Is Debug", myScript.isDebug);
        myScript.gameController = EditorGUILayout.ObjectField("Game Controller", myScript.gameController, typeof(GameController), true) as GameController;
        myScript.whatIsGround = EditorGUILayout.LayerField("What Is Ground", myScript.whatIsGround);
        myScript.spawnPoint = EditorGUILayout.ObjectField("Spawn Point", myScript.spawnPoint, typeof(GameObject), true) as GameObject;
        myScript.jump = EditorGUILayout.ObjectField("Jump Audio", myScript.jump, typeof(AudioClip), true) as AudioClip;
        myScript.death = EditorGUILayout.ObjectField("Death Audio", myScript.death, typeof(AudioClip), true) as AudioClip;
        myScript.trinketOrCheckpoint = EditorGUILayout.ObjectField("Trinket/Checkpoint Audio", myScript.trinketOrCheckpoint, typeof(AudioClip), true) as AudioClip;

        if (myScript.whatIsGround.value != 0)
        {
            myScript.whatIsGround = LayerMask.GetMask(LayerMask.LayerToName(myScript.whatIsGround));
        }

        myScript.facingRight = EditorGUILayout.Toggle("Facing Right", myScript.facingRight);
        myScript.facingUp = EditorGUILayout.Toggle("Facing Up", myScript.facingUp);
    }
}

